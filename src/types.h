/**
 * Space battle 2D game
 *
 * Copyright (C) 2018, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */

#ifndef TYPES_GUARD
#define TYPES_GUARD

struct task_par {
  struct timespec at;
  struct timespec dl;
  int             period, deadline, priority, dmiss;
};

struct creator_wrapper {
  pthread_t       *threads;
  struct task_par *task_par;
  void *(**tasks)(void *);
};

struct sprite {
  BITMAP *bmp;
  float   sx, sy, oldsx, oldsy, vx, vy, ax, ay;
  short   id;
  char    visible;
};

#endif
