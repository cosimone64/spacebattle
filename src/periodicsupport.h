/**
 * Space battle 2D game
 *
 * Copyright (C) 2018, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */

#ifndef PERIODICSUPPORT_GUARD
#define PERIODICSUPPORT_GUARD

float time_cmp(const struct timespec *after, const struct timespec *before);
int   deadline_miss(struct task_par *tp);
void  set_period(struct task_par *tp);
void  threads_join(pthread_t *threads);
void  time_add_ms(struct timespec *t, int ms);
void  time_copy(struct timespec *dest, const struct timespec *src);
void  wait_for_period(struct task_par *tp);
void *threads_init(void *data);

#endif
