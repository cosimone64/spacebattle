/**
 * Space battle 2D game
 *
 * Copyright (C) 2018, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */

#include <allegro.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>

#include "constants.h"
#include "types.h"
#include "aux.h"

#undef MAX
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#undef MIN
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

void aux_pthread_attr_init(pthread_attr_t *attr)
{
  if (pthread_attr_init(attr))
    aux_exit_with_error(ATTR_INIT_ERROR);
}

void aux_pthread_attr_destroy(pthread_attr_t *attr)
{
  if (pthread_attr_destroy(attr))
    aux_exit_with_error(ATTR_DESTROY_ERROR);
}

void aux_pthread_attr_setinheritsched(pthread_attr_t *attr, int inheritsched)
{
  if (pthread_attr_setinheritsched(attr, inheritsched))
    aux_exit_with_error(SETINHERITSCHED_ERROR);
}

void aux_pthread_attr_setschedpolicy(pthread_attr_t *attr, int policy)
{
  if (pthread_attr_setschedpolicy(attr, policy))
    aux_exit_with_error(SETSCHEDPOLICY_ERROR);
}

void aux_pthread_attr_setschedparam(pthread_attr_t           *attr,
                                    const struct sched_param *param)
{
  if (pthread_attr_setschedparam(attr, param))
    aux_exit_with_error(SETPARAM_ERROR);
}

void aux_pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                        void *(*start_routine)(void *), void    *arg)
{
  if (pthread_create(thread, attr, start_routine, arg))
    aux_exit_with_error(THREAD_CREATE_ERROR);
}

void aux_pthread_join(const pthread_t thread, void **retval)
{
  if (pthread_join(thread, retval))
    aux_exit_with_error(THREAD_JOIN_ERROR);
}

double aux_absmax(double x, double y)
{
  return (fabs(x) > fabs(y)) ? x : y;
}

double aux_absmin(double x, double y)
{
  return (fabs(x) < fabs(y)) ? x : y;
}

void aux_sem_wait(sem_t *sem)
{
  if (sem_wait(sem) < 0)
    aux_exit_with_error(SEM_WAIT_ERROR);
}

void aux_sem_post(sem_t *sem)
{
  if (sem_post(sem) < 0)
    aux_exit_with_error(SEM_POST_ERROR);
}

void aux_create_bitmap(struct sprite *s, const char *filename,
                       const char *errmsg)
{
  s->bmp = load_bitmap(filename, NULL);
  if (!s->bmp)
    aux_exit_with_error(errmsg);
  s->visible = true;
}

void aux_exit_with_error(const char *error)
{
  perror(error);
  exit(EXIT_FAILURE);
}
