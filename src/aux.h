/**
 * Space battle 2D game
 *
 * Copyright (C) 2018, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */

#ifndef AUX_GUARD
#define AUX_GUARD

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a)[0])
#define ARRAY_END(a) ((a) + ARRAY_SIZE((a)))

double aux_absmax(double x, double y);
double aux_absmin(double x, double y);
void   aux_create_bitmap(struct sprite *s, const char *filename,
                         const char *errmsg);
void   aux_exit_with_error(const char *error);
void   aux_sem_post(sem_t *sem);
void   aux_sem_wait(sem_t *sem);
void   aux_pthread_attr_destroy(pthread_attr_t *attr);
void   aux_pthread_attr_init(pthread_attr_t *attr);
void aux_pthread_attr_setinheritsched(pthread_attr_t *attr, int inheritsched);
void aux_pthread_attr_setschedparam(pthread_attr_t           *attr,
                                    const struct sched_param *param);
void aux_pthread_attr_setschedpolicy(pthread_attr_t *attr, int policy);
void aux_pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                        void *(*start_routine)(void *), void    *arg);
void aux_pthread_join(pthread_t thread, void **retval);

#endif
