/**
 * Space battle 2D game
 *
 * Copyright (C) 2018, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */

#ifndef CONSTANTS_GUARD
#define CONSTANTS_GUARD

enum { false, true };

enum {
  KB_TASK,
  ENEMY_TASK,
  COLLISION_TASK,
  DRAW_TASK,
  SOUND_TASK,
  POSITION_TASK,
  TASK_NUM
};

enum { SPRITE_SEM, SOUND_SEM, GAME_OVER_SEM, BAR_SEM, SEM_NUM };
enum horizontal_direction { LEFT, RIGHT, NO_HORIZONTAL_DIRECTION };
enum vertical_direction { UP, DOWN, NO_VERTICAL_DIRECTION };
enum sprite_id { ID_PLAYER_SHIP, ID_ENEMY_SHIP, ID_PROJECTILE, OTHER };

/*
 * GUI parameters.
 */
#define HRES 1280
#define VRES 1024
#define COLOR_DEPTH 16
#define TEXT_BG_COLOR -1

#define SHIP_PANEL_TEXT "YOU"
#define ENEMY_PANEL_TEXT "ENEMY"
#define PANEL_COLOR 1
#define BAR_COLOR 65000
#define BAR_HORIZONTAL_LEFT_OFFSET 150
#define HEALTH_MINIMUM BAR_HORIZONTAL_LEFT_OFFSET
#define BAR_HORIZONTAL_RIGHT_OFFSET 20
#define BAR_VERTICAL_OFFSET 20
#define TEXT_BAR_GAP 50
#define PANEL_SIZE ((int) (VRES / 10))
#define HEALTH_BAR_SIZE (HRES - 2 * HEALTH_BAR_OFFSET)
#define HEALTH_BAR_DECREASE_SIZE 20
#define COLLISION_HEALTH_DECREASE 5
#define MIN_BORDER_DISTANCE 5

#define X_MOVE_LIMIT HRES
#define Y_MOVE_LIMIT (VRES - PANEL_SIZE)

#define SHIP_INITIAL_SX ((int) (HRES / 2))
#define SHIP_INITIAL_SY ((int) (VRES - (VRES / 3)))
#define ENEMY_INITIAL_SX ((int) (HRES / 2))
#define ENEMY_INITIAL_SY ((int) (VRES / 3))

/*
 * Periods (in milliseconds).
 */
#define KB_PERIOD 17
#define ENEMY_PERIOD 17
#define COLLISION_PERIOD 17
#define DRAW_PERIOD 17
#define SOUND_PERIOD 17
#define POSITION_PERIOD 17
#define DEADLINE_OFFSET 20

#define TASKS_TO_UNBLOCK 3

/*
 * Error messages.
 */
#define ALLEGRO_INIT_ERROR "Error while initializing Allegro"
#define INSTALL_KEYBOARD_ERROR "Error while installing the keyboard"
#define DEADLINE_MISS_ERROR "Error: a deadline was missed"
#define DL_MISS_MSG " missed its deadline!"
#define SEM_WAIT_ERROR "Error while waiting on semaphore"
#define SEM_POST_ERROR "Error while posting on semaphore"
#define SEM_DESTROY_ERROR "Error while destroying semaphore"
#define SET_GFX_ERROR "Error while setting graphics mode"
#define INSTALL_SOUND_ERROR "Error while initializing sound module"
#define LOAD_SAMPLE_ERROR "Error while loading sound sample"
#define SEM_INIT_ERROR "Error while initializing semaphore"
#define ATTR_INIT_ERROR "Error while initializing thread attributes"
#define ATTR_DESTROY_ERROR "Error while destroying thread attributes"
#define SETINHERITSCHED_ERROR "Error while setting inherited scheduling"
#define SETSCHEDPOLICY_ERROR "Error while setting scheduling policy"
#define SETPARAM_ERROR "Error while setting creator thread parameters"
#define THREAD_CREATE_ERROR "Error while creating thread"
#define THREAD_CREATE_ERROR_DRAW "Error while creating draw thread"
#define THREAD_CREATE_ERROR_ENEMY "Error while creating enemy thread"
#define THREAD_CREATE_ERROR_SHIP "Error while creating ship thread"
#define THREAD_CREATE_ERROR_PROJECTILE "Error while creating projectile thread"
#define CLOCK_SET_ERROR "Error while setting clock"
#define THREAD_JOIN_ERROR "Error while joining thread"
#define BG_BMP_NOT_FOUND "Error while loading background image"
#define SHIP_BMP_NOT_FOUND "Error while loading ship image"
#define ENEMY_BMP_NOT_FOUND "Error while loading enemy image"
#define SCREEN_BUFFER_CREATE_ERROR "Error while creating screen buffer"
#define BG_BUFFER_CREATE_ERROR "Error while creating background buffer"
#define PROJ_GENERATE_ERROR "Error while generating projectile"
#define TOO_MANY_PROJECTILES_ERROR                                            \
  "Projectile amount limit reached: "                                         \
  "cannot allocate"
#define SHIP_OUT_OF_RANGE_ERROR "Error: specified ship does not exist"

/*
 * Filenames.
 */
#define ASSETS "/home/cosimone/workspaces/spacebattle/assets/"
#define BG_BMP_FILENAME ASSETS "background.bmp"
#define SHIP_BMP_FILENAME ASSETS "ship.bmp"
#define ENEMY_BMP_FILENAME ASSETS "enemy.bmp"
#define PROJ_BMP_FILENAME ASSETS "projectile.bmp"
#define PROJECTILE_SAMPLE_FILENAME ASSETS "projectile.wav"

/*
 * Bitmap-related constants.
 */
#define MAX_SPRITE_NUM 256
#define MAX_SOUND_SAMPLE_NUM 512

/*
 * Ship indices to index arrays.
 */
#define SHIP_SPRITE 0
#define ENEMY_SPRITE 1
#define SHIP_NUM 2
#define FIRST_PROJ_IDX 2

/*
 * Ship constants.
 */
#define SHIP_START_X 400
#define SHIP_START_Y 500
#define SHOT_DELAY 200000

/*
 * Enemy ship constants.
 */
#define ENEMY_START_X 800
#define ENEMY_START_Y 100
#define ENEMY_VERTICAL_MOVE_BOUND 30

/*
 * Projectile constants.
 */
#define PROJ_SHOOT_OFFSET 20

/*
 * Sound constants.
 */
#define VOLUME 100
#define PAN 128
#define FREQ 1000

/*
 * Physics parameters.
 */
#define ACCELERATION 1000
#define COLLISION_ACCELERATION (ACCELERATION + 100)

#define MAX_VELOCITY 300
#define BOUND_COLLISION_VELOCITY 100
#define PROJECTILE_VELOCITY (MAX_VELOCITY + 150)
#define ZERO_CUTOFF 0.00000000000001
#define DAMP 0.99
#endif
