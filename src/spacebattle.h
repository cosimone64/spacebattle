/**
 * Space battle 2D game
 *
 * Copyright (C) 2018, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */

#ifndef SPACEBATTLE_GUARD
#define SPACEBATTLE_GUARD

double update_velocity(double new_s, double v, double a, double t, int bound);
int    check_game_over(void);
int    get_bar_limit(size_t ship);
int    have_collided(const struct sprite *s, const struct sprite *t);
int    is_outside_of_screen(const struct sprite *s);
int is_player_ship_left(const struct sprite *ship, const struct sprite *enemy);
size_t update_projectile_index(size_t index);
void   decrease_bar_limit(size_t ship, int val);
void   detect_projectile_collision(void);
void   detect_ship_collision(void);
void   enemy_move_vertical(void);
void   enemy_shoot_projectile(void);
void   enqueue_sound(SAMPLE *sample);
void   get_projectile_params(struct sprite *s, struct sprite *proj);
void   perform_key_action(char key);
void   position_compute_loop(double t, double t2);
void   projectile_check_hit(struct sprite *proj);
void   set_game_over(int val);
void   shoot_projectile(int shooting_ship_idx);
void   sprite_move(struct sprite *s, int hdir, int vdir);
void  *collision_task(void *data);
void  *keyboard_task(void *data);
void  *position_task(void *data);
void  *sound_task(void *data);
void   clocks_init(void);
void   draw_panel(BITMAP *buf);
void   draw_single_sprite(BITMAP *bg, BITMAP *buf, struct sprite *s);
void   draw_sprites(BITMAP *bg, BITMAP *buf);
void   load_global_bitmaps(void);
void   spacebattle_finalize(void);
void   spacebattle_init(void);
void   spacebattle_init_semaphores(void);
void   spacebattle_init_sound(void);
void  *draw_task(void *data);
void  *enemy_task(void *data);

#endif
