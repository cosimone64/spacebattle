/**
 * Space battle 2D game
 *
 * Copyright (C) 2018, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */

#define _POSIX_SOURCE
#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 500

#include <allegro.h>
#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <time.h>

#include "constants.h"
#include "types.h"
#include "aux.h"
#include "periodicsupport.h"
#include "spacebattle.h"

#define SOUND

/**
 * Contains relevant information for every sprite on the screen.
 */
static struct sprite sprites[MAX_SPRITE_NUM] = {0};

/**
 * Flags program termination.
 */
static char game_over = false;

/**
 * All semaphores are stored in an array, for convenience.
 */
static sem_t semaphores[SEM_NUM] = {0};

#ifdef SOUND
/**
 * Queue containing sound samples to be played.
 */
static struct {
  SAMPLE *array[MAX_SOUND_SAMPLE_NUM];
  size_t  end_index;
} sound_samples = {0};

/**
 * Sound to be played when a shot is fired.
 */
static SAMPLE *projectile_sound = NULL;
#endif

/**
 * Denotes the time of the last fired shot to limit fire ratio.
 */
static struct timespec first_shot_times[2] = {0};

/**
 * Player and enemy ship health bars.
 */
static int bar_limits[SHIP_NUM] = {BAR_HORIZONTAL_RIGHT_OFFSET,
                                   BAR_HORIZONTAL_RIGHT_OFFSET};

int main(void)
{
  void *(*tasks[TASK_NUM])(void *) = {keyboard_task,  enemy_task,
                                      collision_task, draw_task,
                                      sound_task,     position_task};
  struct task_par        task_par[TASK_NUM];
  pthread_t              threads[TASK_NUM];
  pthread_t              creator_thread;
  struct creator_wrapper wrapper;
  struct sched_param     sched = {30};
  pthread_attr_t         creator_attr;

  wrapper.threads  = threads;
  wrapper.task_par = task_par;
  wrapper.tasks    = tasks;

  spacebattle_init();
  aux_pthread_attr_init(&creator_attr);
  aux_pthread_attr_setinheritsched(&creator_attr, PTHREAD_EXPLICIT_SCHED);
  aux_pthread_attr_setschedpolicy(&creator_attr, SCHED_FIFO);
  aux_pthread_attr_setschedparam(&creator_attr, &sched);
  aux_pthread_create(&creator_thread, NULL, threads_init, (void *) &wrapper);
  aux_pthread_join(creator_thread, NULL);
  aux_pthread_attr_destroy(&creator_attr);
  threads_join(threads);
  spacebattle_finalize();
  return 0;
}

void spacebattle_init(void)
{
  int i;

  if (allegro_init())
    aux_exit_with_error(ALLEGRO_INIT_ERROR);
  set_color_depth(COLOR_DEPTH);
  if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, HRES, VRES, 0, 0))
    aux_exit_with_error(SET_GFX_ERROR);
  spacebattle_init_semaphores();

#ifdef SOUND
  if (install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0))
    aux_exit_with_error(INSTALL_SOUND_ERROR);
  if (!(projectile_sound = load_sample(PROJECTILE_SAMPLE_FILENAME)))
    aux_exit_with_error(LOAD_SAMPLE_ERROR);
#endif
  for (i = 0; i < SHIP_NUM; ++i)
    clock_gettime(CLOCK_MONOTONIC, &first_shot_times[i]);
  if (install_keyboard())
    aux_exit_with_error(INSTALL_KEYBOARD_ERROR);
  set_keyboard_rate(1, 20);
  srand(time(NULL));
}

void spacebattle_init_semaphores(void)
{
  sem_t *sem;

  /*
   * Sprite semaphore is initialized with a post value of 0,
   * since threads have to wait for the draw task to load
   * bitmaps.
   */
  if (sem_init(&semaphores[SPRITE_SEM], 0, 0) < 0)
    aux_exit_with_error(SEM_INIT_ERROR);

  for (sem = &semaphores[1]; sem < ARRAY_END(semaphores); ++sem) {
    if (sem_init(sem, 0, 1) < 0)
      aux_exit_with_error(SEM_INIT_ERROR);
  }
}

void spacebattle_finalize(void)
{
  const struct sprite *sp;
  sem_t               *sem;

  for (sp = sprites; sp < ARRAY_END(sprites); ++sp) {
    if (sp->bmp)
      destroy_bitmap(sp->bmp);
  }

  for (sem = semaphores; sem < ARRAY_END(semaphores); ++sem) {
    if (sem_destroy(sem) < 0)
      aux_exit_with_error(SEM_DESTROY_ERROR);
  }

#ifdef SOUND
  destroy_sample(projectile_sound);
#endif
  allegro_exit();
}

void *draw_task(void *data)
{
  const char      *task_name = "Draw task";
  struct task_par *tp        = data;
  sem_t           *sem       = &semaphores[SPRITE_SEM];
  BITMAP          *background, *screen_buf;
  int              i;

  screen_buf = create_bitmap(HRES, VRES);
  if (!screen_buf)
    aux_exit_with_error(SCREEN_BUFFER_CREATE_ERROR);

  background = load_bitmap(BG_BMP_FILENAME, NULL);
  if (!background)
    aux_exit_with_error(BG_BMP_NOT_FOUND);
  aux_create_bitmap(&sprites[SHIP_SPRITE], (char *) SHIP_BMP_FILENAME,
                    SHIP_BMP_NOT_FOUND);
  sprites[SHIP_SPRITE].id = ID_PLAYER_SHIP;

  aux_create_bitmap(&sprites[ENEMY_SPRITE], (char *) ENEMY_BMP_FILENAME,
                    ENEMY_BMP_NOT_FOUND);
  sprites[ENEMY_SPRITE].id = ID_ENEMY_SHIP;

  for (i = 0; i < TASKS_TO_UNBLOCK; ++i)
    aux_sem_post(sem);
  /* Load background. */
  blit(background, screen_buf, 0, 0, 0, 0, background->w, background->h);
  set_period(tp);
  while (!check_game_over()) {
    aux_sem_wait(sem);
    draw_sprites(background, screen_buf);
    aux_sem_post(sem);
    if (deadline_miss(tp))
      fprintf(stderr, "%s%s\n", task_name, DL_MISS_MSG);
    wait_for_period(tp);
  }
  destroy_bitmap(screen_buf);
  return NULL;
}

void draw_sprites(BITMAP *bg, BITMAP *buf)
{
  struct sprite *s;

  for (s = sprites; s < ARRAY_END(sprites); ++s) {
    if (s->bmp)
      draw_single_sprite(bg, buf, s);
  }
  draw_panel(buf);
  scare_mouse();
  /* Copy buffer to screen. */
  blit(buf, screen, 0, 0, 0, 0, buf->w, buf->h);
  unscare_mouse();
}

void draw_single_sprite(BITMAP *bg, BITMAP *buf, struct sprite *s)
{
  /* Restore old background. */
  blit(bg, buf, (int) s->oldsx, (int) s->oldsy, (int) s->oldsx, (int) s->oldsy,
       s->bmp->w, s->bmp->h);
  if (!s->visible) {
    destroy_bitmap(s->bmp);
    s->bmp = NULL;
  } else {
    /* Draw sprite. */
    masked_blit(s->bmp, buf, 0, 0, (int) s->sx, (int) s->sy, s->bmp->w,
                s->bmp->h);
  }
  s->oldsx = s->sx;
  s->oldsy = s->sy;
}

void draw_panel(BITMAP *buf)
{
  int ship_bar_upper  = abs(VRES - (3 * BAR_VERTICAL_OFFSET));
  int ship_bar_lower  = abs(VRES - (4 * BAR_VERTICAL_OFFSET));
  int enemy_bar_upper = abs(VRES - BAR_VERTICAL_OFFSET);
  int enemy_bar_lower = abs(VRES - (2 * BAR_VERTICAL_OFFSET));
  int ship_text_x     = abs(BAR_HORIZONTAL_LEFT_OFFSET - TEXT_BAR_GAP);
  int ship_text_y     = abs(VRES - (4 * BAR_VERTICAL_OFFSET));
  int enemy_text_x    = abs(BAR_HORIZONTAL_LEFT_OFFSET - TEXT_BAR_GAP);
  int enemy_text_y    = abs(VRES - (2 * BAR_VERTICAL_OFFSET));
  int ship_limit      = get_bar_limit(SHIP_SPRITE);
  int enemy_limit     = get_bar_limit(ENEMY_SPRITE);

  rectfill(buf, 0, VRES, HRES, VRES - PANEL_SIZE, PANEL_COLOR);
  rectfill(buf, BAR_HORIZONTAL_LEFT_OFFSET, ship_bar_upper, HRES - ship_limit,
           ship_bar_lower, BAR_COLOR);
  rectfill(buf, BAR_HORIZONTAL_LEFT_OFFSET, enemy_bar_upper,
           HRES - enemy_limit, enemy_bar_lower, BAR_COLOR);
  textout_ex(buf, font, SHIP_PANEL_TEXT, ship_text_x, ship_text_y, BAR_COLOR,
             TEXT_BG_COLOR);
  textout_ex(buf, font, ENEMY_PANEL_TEXT, enemy_text_x, enemy_text_y,
             BAR_COLOR, TEXT_BG_COLOR);
}

void *enemy_task(void *data)
{
  const char          *task_name = "Enemy task";
  struct task_par     *tp        = data;
  const struct sprite *ship      = &sprites[SHIP_SPRITE];
  struct sprite       *enemy     = &sprites[ENEMY_SPRITE];

  /* Wait for bitmaps to be loaded. */
  aux_sem_wait(&semaphores[SPRITE_SEM]);
  enemy->sx = ENEMY_INITIAL_SX;
  enemy->sy = ENEMY_INITIAL_SY;
  set_period(tp);
  while (!check_game_over()) {
    int hdir = is_player_ship_left(ship, enemy) ? LEFT : RIGHT;
    sprite_move(&sprites[ENEMY_SPRITE], hdir, NO_VERTICAL_DIRECTION);
    enemy_move_vertical();
    enemy_shoot_projectile();
    if (deadline_miss(tp))
      fprintf(stderr, "%s%s\n", task_name, DL_MISS_MSG);
    wait_for_period(tp);
  }
  return NULL;
}

int is_player_ship_left(const struct sprite *ship, const struct sprite *enemy)
{
  int is_left;

  aux_sem_wait(&semaphores[SPRITE_SEM]);
  is_left = ship->sx <= enemy->sx;
  aux_sem_post(&semaphores[SPRITE_SEM]);
  return is_left;
}

static int get_random_vdir(void)
{
  switch (rand() % 3) {
  case 0:
    return UP;
  case 1:
    return DOWN;
  default:
    return NO_VERTICAL_DIRECTION;
  }
}

static int get_enemy_vertical_direction(double enemy_sy, int vertical_bound)
{
  if (enemy_sy >= vertical_bound)
    return UP;
  if (enemy_sy <= 0)
    return DOWN;
  return get_random_vdir();
}

void enemy_move_vertical(void)
{
  struct sprite *ship, *enemy;
  int            vdir, bound;

  aux_sem_wait(&semaphores[SPRITE_SEM]);
  ship = &sprites[SHIP_SPRITE];

  bound = abs((int) ship->sy - (ship->bmp->h + ENEMY_VERTICAL_MOVE_BOUND)) / 2;
  aux_sem_post(&semaphores[SPRITE_SEM]);

  enemy = &sprites[ENEMY_SPRITE];
  vdir  = get_enemy_vertical_direction(enemy->sy, bound);
  sprite_move(enemy, NO_HORIZONTAL_DIRECTION, vdir);
}

void enemy_shoot_projectile(void)
{
  int percent                     = 10;
  int shoot_probability_threshold = 8;
  int randval                     = rand() % percent;

  if (randval >= shoot_probability_threshold)
    shoot_projectile(ENEMY_SPRITE);
}

void *position_task(void *data)
{
  const char      *task_name = "Position task";
  double           t         = POSITION_PERIOD * 0.001;
  double           t2        = t * t;
  struct task_par *tp        = data;

  set_period(tp);
  while (!check_game_over()) {
    aux_sem_wait(&semaphores[SPRITE_SEM]);
    position_compute_loop(t, t2);
    aux_sem_post(&semaphores[SPRITE_SEM]);
    if (deadline_miss(tp))
      fprintf(stderr, "%s%s\n", task_name, DL_MISS_MSG);
    wait_for_period(tp);
  }
  return NULL;
}

void position_compute_loop(double t, double t2)
{
  struct sprite *s;

  for (s = sprites; s < ARRAY_END(sprites); ++s) {
    if (s->visible) {
      double accel_val = 0.5;
      /* Horizontal and vertical bounds, to determine screen
       * limit. */
      int hbound = abs(HRES - s->bmp->w);
      int vbound = abs(Y_MOVE_LIMIT - s->bmp->h);

      s->sx = accel_val * s->ax * t2 + s->vx * t + s->sx;
      s->sy = accel_val * s->ay * t2 + s->vy * t + s->sy;
      s->vx = update_velocity(s->sx, s->vx, s->ax, t, hbound);
      s->vy = update_velocity(s->sy, s->vy, s->ay, t, vbound);
    }
  }
}

double update_velocity(double new_s, double old_v, double old_a, double t,
                       int bound)
{
  double new_v = old_a * t + old_v;

  if (new_v > ZERO_CUTOFF && new_s > 0 && new_s < bound)
    return aux_absmin(new_v, MAX_VELOCITY);
  if (new_v < -ZERO_CUTOFF && new_s > 0 && new_s < bound)
    return aux_absmin(new_v, -MAX_VELOCITY);
  /* If the sprite is touching an invisible wall, it bounces on it. */
  return -DAMP * new_v;
}

int have_collided(const struct sprite *s, const struct sprite *t)
{
  double s_right  = s->sx + s->bmp->w;
  double s_bottom = s->sy + s->bmp->h;
  double t_right  = t->sx + t->bmp->w;
  double t_bottom = t->sy + t->bmp->h;

  if (s_right < t->sx)
    return 0;
  if (s->sx > t_right)
    return 0;
  if (s->sy >= t_bottom)
    return 0;
  return s_bottom >= t->sy;
}

static void handle_sprite_collision(struct sprite *s, struct sprite *t)
{
  double old_s_vx = s->vx, old_s_vy = s->vy;

  s->vx = DAMP * t->vx;
  s->vy = DAMP * t->vy;
  t->vx = DAMP * old_s_vx;
  t->vy = DAMP * old_s_vy;

  s->ax = 0;
  s->ay = 0;
  t->ax = 0;
  t->ay = 0;
}

void *collision_task(void *data)
{
  const char      *task_name = "Collision task";
  struct task_par *tp        = data;

  set_period(tp);
  aux_sem_wait(&semaphores[SPRITE_SEM]);
  while (!check_game_over()) {
    struct sprite *ship, *enemy, *proj;

    aux_sem_wait(&semaphores[SPRITE_SEM]);
    ship  = &sprites[SHIP_SPRITE];
    enemy = &sprites[ENEMY_SPRITE];

    if (have_collided(ship, enemy))
      handle_sprite_collision(ship, enemy);
    for (proj = &sprites[FIRST_PROJ_IDX]; proj < ARRAY_END(sprites); ++proj) {
      if (proj->bmp)
        projectile_check_hit(proj);
    }
    aux_sem_post(&semaphores[SPRITE_SEM]);
    if (deadline_miss(tp))
      fprintf(stderr, "%s%s\n", task_name, DL_MISS_MSG);
    wait_for_period(tp);
  }
  return NULL;
}

int is_outside_of_screen(const struct sprite *s)
{
  if (s->sy >= (Y_MOVE_LIMIT - (s->bmp->h + MIN_BORDER_DISTANCE)))
    return 1;
  if (s->sy <= MIN_BORDER_DISTANCE)
    return 1;
  if (s->sx <= MIN_BORDER_DISTANCE)
    return 1;
  return (s->sx + s->bmp->w) >= (HRES - MIN_BORDER_DISTANCE);
}

void projectile_check_hit(struct sprite *proj)
{
  int player_dead =
    abs(HRES - bar_limits[SHIP_SPRITE]) <= BAR_HORIZONTAL_LEFT_OFFSET;
  int enemy_dead =
    abs(HRES - bar_limits[ENEMY_SPRITE]) <= BAR_HORIZONTAL_LEFT_OFFSET;

  if (have_collided(&sprites[SHIP_SPRITE], proj)) {
    decrease_bar_limit(SHIP_SPRITE, HEALTH_BAR_DECREASE_SIZE);
    proj->visible = false;
  }
  if (have_collided(&sprites[ENEMY_SPRITE], proj)) {
    decrease_bar_limit(ENEMY_SPRITE, HEALTH_BAR_DECREASE_SIZE);
    proj->visible = false;
  }
  if (player_dead || enemy_dead)
    set_game_over(true);
  if (is_outside_of_screen(proj))
    proj->visible = false;
}

void sprite_move(struct sprite *s, int hdir, int vdir)
{
  aux_sem_wait(&semaphores[SPRITE_SEM]);
  if (hdir != NO_HORIZONTAL_DIRECTION) {
    int sign = (hdir == LEFT) ? -1 : 1;
    s->ax    = ACCELERATION * sign;
  }
  if (vdir != NO_VERTICAL_DIRECTION) {
    int sign = (vdir == UP) ? -1 : 1;
    s->ay    = ACCELERATION * sign;
  }
  aux_sem_post(&semaphores[SPRITE_SEM]);
}

void shoot_projectile(int shooting_ship_idx)
{
  static struct timespec current_shot_times[2];
  static int             new_proj_idx = 2;

  struct timespec *current_shot_time = &current_shot_times[shooting_ship_idx];
  struct timespec *first_shot_time   = &first_shot_times[shooting_ship_idx];
  sem_t           *semaphore         = &semaphores[SPRITE_SEM];

  struct sprite *proj, *ship;
  double         diff;
  int            sign;

  /* Compare current shot time to the last time a shot was fired. */
  clock_gettime(CLOCK_MONOTONIC, current_shot_time);
  diff = time_cmp(current_shot_time, first_shot_time);
  if (diff < SHOT_DELAY)
    return;

  /* Save current shot time to compare it to the next one. */
  clock_gettime(CLOCK_MONOTONIC, first_shot_time);
  aux_sem_wait(semaphore);

  proj = &sprites[new_proj_idx];
  aux_create_bitmap(proj, PROJ_BMP_FILENAME, PROJ_GENERATE_ERROR);

  ship = &sprites[shooting_ship_idx];
  get_projectile_params(ship, &sprites[new_proj_idx]);

  sign = (shooting_ship_idx == SHIP_SPRITE) ? -1 : 1;

  proj->sx = ship->sx + ship->bmp->w / 2.0 - proj->bmp->w / 2.0;
  proj->sy = ship->sy + sign * PROJ_SHOOT_OFFSET;
  if (shooting_ship_idx != SHIP_SPRITE)
    proj->sy += ship->bmp->h;
  proj->vy = ship->vy + sign * PROJECTILE_VELOCITY;
  proj->vx = ship->vx;
  proj->ax = proj->ay = 0;

  new_proj_idx = update_projectile_index(new_proj_idx);
  aux_sem_post(semaphore);
#ifdef SOUND
  enqueue_sound(projectile_sound);
#endif
}

void get_projectile_params(struct sprite *ship, struct sprite *proj)
{
  int sign = (ship->id == ID_PLAYER_SHIP) ? -1 : 1;

  proj->sx = ship->sx + ship->bmp->w / 2.0 - proj->bmp->w / 2.0;
  proj->sy = ship->sy + sign * PROJ_SHOOT_OFFSET;
  if (ship != SHIP_SPRITE)
    proj->sy += ship->bmp->h;
  proj->vy = ship->vy + sign * PROJECTILE_VELOCITY;
  proj->vx = ship->vx;
  proj->ax = proj->ay = 0;
}

size_t update_projectile_index(size_t idx)
{
  size_t old_idx      = idx;
  size_t max_proj_num = MAX_SPRITE_NUM - FIRST_PROJ_IDX;

  do {
    idx = (idx - 1) % max_proj_num + FIRST_PROJ_IDX;
  } while (old_idx != idx && sprites[idx].bmp);
  if (old_idx == idx)
    aux_exit_with_error(TOO_MANY_PROJECTILES_ERROR);
  return idx;
}

#ifdef SOUND
void enqueue_sound(SAMPLE *sample)
{
  sem_t *s = &semaphores[SOUND_SEM];

  aux_sem_wait(s);
  sound_samples.array[sound_samples.end_index] = sample;
  sound_samples.end_index =
    (sound_samples.end_index + 1) % MAX_SOUND_SAMPLE_NUM;
  aux_sem_post(s);
}
#endif

void *keyboard_task(void *data)
{
  const char      *task_name = "Keyboard task";
  struct task_par *tp        = data;
  struct sprite   *ship      = &sprites[SHIP_SPRITE];
  sem_t           *sem       = &semaphores[SPRITE_SEM];

  aux_sem_wait(sem);
  ship->sx = SHIP_INITIAL_SX;
  ship->sy = SHIP_INITIAL_SY;
  aux_sem_post(sem);
  set_period(tp);
  while (!check_game_over()) {
    if (keypressed()) {
      int shift_val = 8;
      perform_key_action(readkey() >> shift_val);
    } else {
      aux_sem_wait(sem);
      ship->ax = 0;
      ship->ay = 0;
      aux_sem_post(sem);
    }
    if (deadline_miss(tp))
      fprintf(stderr, "%s%s\n", task_name, DL_MISS_MSG);
    wait_for_period(tp);
  }
  return NULL;
}

void perform_key_action(char key)
{
  switch (key) {
  case KEY_LEFT:
  case KEY_H:
  case KEY_A:
    sprite_move(&sprites[SHIP_SPRITE], LEFT, NO_VERTICAL_DIRECTION);
    break;
  case KEY_RIGHT:
  case KEY_L:
  case KEY_D:
    sprite_move(&sprites[SHIP_SPRITE], RIGHT, NO_VERTICAL_DIRECTION);
    break;
  case KEY_UP:
  case KEY_K:
  case KEY_W:
    sprite_move(&sprites[SHIP_SPRITE], NO_HORIZONTAL_DIRECTION, UP);
    break;
  case KEY_DOWN:
  case KEY_J:
  case KEY_S:
  case KEY_X:
    sprite_move(&sprites[SHIP_SPRITE], NO_HORIZONTAL_DIRECTION, DOWN);
    break;
  case KEY_Q:
    sprite_move(&sprites[SHIP_SPRITE], LEFT, UP);
    break;
  case KEY_E:
    sprite_move(&sprites[SHIP_SPRITE], RIGHT, UP);
    break;
  case KEY_Z:
    sprite_move(&sprites[SHIP_SPRITE], LEFT, DOWN);
    break;
  case KEY_C:
    sprite_move(&sprites[SHIP_SPRITE], RIGHT, DOWN);
    break;
  case KEY_SPACE:
    shoot_projectile(SHIP_SPRITE);
    break;
  case KEY_ESC:
    set_game_over(true);
    break;
  default:
    break;
  }
}

void *sound_task(void *data)
{
#ifdef SOUND
  const char *task_name = "Sound task";
  size_t      head      = 0;
#endif
  struct task_par *tp  = data;
  sem_t           *sem = &semaphores[SOUND_SEM];

  set_period(tp);
  while (!check_game_over()) {
#ifdef SOUND
    aux_sem_wait(sem);
    while (head != sound_samples.end_index) {
      play_sample(sound_samples.array[head], VOLUME, PAN, FREQ, 0);
      head = (head + 1) % MAX_SOUND_SAMPLE_NUM;
    }
    aux_sem_post(sem);
    if (deadline_miss(tp))
      fprintf(stderr, "%s%s\n", task_name, DL_MISS_MSG);
    wait_for_period(tp);
#endif
  }
  return NULL;
}

int check_game_over(void)
{
  sem_t *s = &semaphores[GAME_OVER_SEM];
  int    ret;

  aux_sem_wait(s);
  ret = game_over;
  aux_sem_post(s);
  return ret;
}

void set_game_over(int val)
{
  sem_t *s = &semaphores[GAME_OVER_SEM];
  aux_sem_wait(s);
  game_over = val;
  aux_sem_post(s);
}

int get_bar_limit(size_t ship)
{
  sem_t *s = &semaphores[BAR_SEM];
  int    ret;

  aux_sem_wait(s);
  ret = bar_limits[ship];
  aux_sem_post(s);
  return ret;
}

void decrease_bar_limit(size_t ship, int val)
{
  sem_t *s = &semaphores[BAR_SEM];
  aux_sem_wait(s);
  bar_limits[ship] += val;
  aux_sem_post(s);
}
