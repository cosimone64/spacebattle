/**
 * Space battle 2D game
 *
 * Copyright (C) 2018, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */

#define _POSIX_SOURCE
#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 500

#include <allegro.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <time.h>

#include "constants.h"
#include "types.h"
#include "aux.h"
#include "periodicsupport.h"

void *threads_init(void *data)
{
  struct creator_wrapper *wrapper  = (struct creator_wrapper *) data;
  struct task_par        *task_par = wrapper->task_par;
  pthread_t              *threads  = wrapper->threads;
  void *(**tasks)(void *)          = wrapper->tasks;
  int periods[TASK_NUM] = {KB_PERIOD,   ENEMY_PERIOD, COLLISION_PERIOD,
                           DRAW_PERIOD, SOUND_PERIOD, COLLISION_PERIOD};
  int i;

  for (i = 0; i < TASK_NUM; ++i) {
    task_par[i].period   = periods[i];
    task_par[i].deadline = periods[i] + DEADLINE_OFFSET;
  }
  for (i = 0; i < TASK_NUM; ++i)
    aux_pthread_create(&threads[i], NULL, tasks[i], (void *) &task_par[i]);
  return NULL;
}

void threads_join(pthread_t *threads)
{
  int i;
  for (i = 0; i < TASK_NUM; ++i)
    aux_pthread_join(threads[i], NULL);
}

void time_copy(struct timespec *dest, const struct timespec *src)
{
  dest->tv_sec  = src->tv_sec;
  dest->tv_nsec = src->tv_nsec;
}

void time_add_ms(struct timespec *t, int ms)
{
  t->tv_sec += ms / 1000;
  t->tv_nsec += (ms % 1000) * 1000000;
  if (t->tv_nsec > 1000000000) {
    t->tv_nsec -= 1000000000;
    t->tv_sec += 1;
  }
}

float time_cmp(const struct timespec *after, const struct timespec *before)
{
  time_t sec_diff  = after->tv_sec - before->tv_sec;
  time_t nsec_diff = after->tv_nsec - before->tv_nsec;

  return sec_diff * 1000000 + nsec_diff / 1000.0;
}

void set_period(struct task_par *tp)
{
  struct timespec t;

  clock_gettime(CLOCK_MONOTONIC, &t);
  time_copy(&tp->at, &t);
  time_copy(&tp->dl, &t);
  time_add_ms(&tp->at, tp->period);
  time_add_ms(&tp->dl, tp->deadline);
}

void wait_for_period(struct task_par *tp)
{
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &tp->at, NULL);
  time_add_ms(&(tp->at), tp->period);
  time_add_ms(&(tp->dl), tp->period);
}

int deadline_miss(struct task_par *tp)
{
  int             was_deadline_missed = false;
  struct timespec now;

  clock_gettime(CLOCK_MONOTONIC, &now);
  if (time_cmp(&now, &tp->dl) > 0) {
    tp->dmiss++;
    was_deadline_missed = true;
  }
  return was_deadline_missed;
}
