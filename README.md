# spacebattle

A small game made using plain C, Allegro and relying on pthreads for
concurrency.
Events, graphics, audio and controls are all managed by separate
concurrent, real time tasks.
